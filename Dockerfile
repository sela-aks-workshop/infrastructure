FROM ubuntu:19.10

# Set Terraform Version
ARG TERRAFORM_VERSION=0.12.23 

# Install Azure CLI and Terraform
RUN apt-get update && \
    apt-get install -y curl wget unzip jq && \
    curl -sL https://aka.ms/InstallAzureCLIDeb | bash && \
    wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
    unzip ./terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /usr/local/bin/

# Set Azure Credentials
ENV AZURE_USERNAME=''
ENV AZURE_PASSWORD=''
ENV AZURE_LOCATION=''
ENV AZURE_SUBSCRIPTION_ID=''
ENV AZURE_RESOURCE_GROUP_NAME=''
ENV AZURE_SERVICE_PRINCIPAL_NAME=''
ENV TOTAL_STUDENTS=''
ENV TF_LOG=''

# Copy Init Script
WORKDIR /tmp
COPY deploy.sh .
COPY cleanup.sh .

# Run Init Script
ENTRYPOINT [ "./deploy.sh" ]