# AKS Workshop
---

## Provisioning

 - To create the course environment we need to configure and deploy the terraform template located in the terraform directory. For a fast, easy and clean deployment just run the command below (from the repository root directory):

```
docker build -t aks-workshop-provisioning:latest . && \
docker run -it \
    -e AZURE_USERNAME='username@email.com' \
    -e AZURE_PASSWORD='xxxxxxxx' \
    -e AZURE_LOCATION='westus' \
    -e AZURE_SUBSCRIPTION_ID='xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx' \
    -e AZURE_RESOURCE_GROUP_NAME='aks-workshop' \
    -e AZURE_SERVICE_PRINCIPAL_NAME='http://aks-workshop' \
    -e TOTAL_STUDENTS='2' \
    -v $(pwd)/terraform/:/terraform/ \
    aks-workshop-provisioning:latest
```

## Decommision

 - To delete the environment after the course just run:

```
docker build -t aks-workshop-provisioning:latest . && \
docker run -it \
    -e AZURE_USERNAME='username@email.com' \
    -e AZURE_PASSWORD='xxxxxxxx' \
    -e AZURE_SUBSCRIPTION_ID='xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx' \
    -e AZURE_RESOURCE_GROUP_NAME='aks-workshop' \
    -e AZURE_SERVICE_PRINCIPAL_NAME='http://aks-workshop' \
    -v $(pwd)/terraform/:/terraform/ \
    --entrypoint="./cleanup.sh" \
    aks-workshop-provisioning:latest
```