#!/bin/bash

# exit when any command fails
set -e

# Login with Azure CLI
echo "-------------------------------"
echo "Step 1: Login to Azure"
Login=$(az login -u ${AZURE_USERNAME} -p ${AZURE_PASSWORD})
az account set --subscription ${AZURE_SUBSCRIPTION_ID}

# Delete Resource Group
echo "-------------------------------"
echo "Step 2: Delete Resource Group"
az group delete -n ${AZURE_RESOURCE_GROUP_NAME} --subscription ${AZURE_SUBSCRIPTION_ID} --yes

# Delete Service Principal
echo "-------------------------------"
echo "Step 3: Delete Service Principal"
az ad sp delete --id ${AZURE_SERVICE_PRINCIPAL_NAME}

# Confirmation Message
echo "-------------------------------"
echo "AKS Workshop Environment Successfully Deleted"