# # Configure the Azure Provider
# https://www.terraform.io/docs/providers/azurerm/index.html
provider "azurerm" {
  subscription_id = var.subscription_id
  client_id       = var.aks_client_id
  client_secret   = var.aks_client_secret
  tenant_id       = var.tenant_id
  version         =  "=2.0.0"
  features {}
}

# Create a resource group if it doesn’t exist
resource "azurerm_resource_group" "aksrg" {
  name     = var.resource_group_name
  location = var.location
  tags     = var.tags
}

# Create virtual network for VMs
resource "azurerm_virtual_network" "vmsvnet" {
  name                = "vms-vNet"
  address_space       = ["10.0.0.0/16"]
  location            = var.location
  resource_group_name = azurerm_resource_group.aksrg.name

  tags = var.tags
}

# Create subnet for VMs
resource "azurerm_subnet" "vmssubnet" {
  name                 = "vms-Subnet"
  resource_group_name  = azurerm_resource_group.aksrg.name
  virtual_network_name = azurerm_virtual_network.vmsvnet.name
  address_prefix       = "10.0.1.0/24"
}

# Create public IP for each VM
resource "azurerm_public_ip" "vmspublicip" {
  count               = var.resource_count
  name                = "${var.cluster_name}-${count.index}-PublicIP"
  location            = var.location
  resource_group_name = azurerm_resource_group.aksrg.name
  allocation_method   = "Static"

  tags = var.tags
}

# Create Network Security Group and rule to allow SSH into the VM
resource "azurerm_network_security_group" "vmssecuritygroup" {
  name                = "vms-NetworkSecurityGroup"
  location            = var.location
  resource_group_name = azurerm_resource_group.aksrg.name

  security_rule {
    name                       = "SSH"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = var.tags
}

# Create network interface
resource "azurerm_network_interface" "vmsnic" {
  count                     = var.resource_count
  name                      = "${var.cluster_name}-${count.index}-NIC"
  location                  = var.location
  resource_group_name       = azurerm_resource_group.aksrg.name

  ip_configuration {
    name                          = "vms-NicConfiguration"
    subnet_id                     = azurerm_subnet.vmssubnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.vmspublicip[count.index].id
  }

  tags = var.tags
}

# Generate random text for a unique storage account name
resource "random_id" "randomId" {
  keepers = {
    # Generate a new ID only when a new resource group is defined
    resource_group = azurerm_resource_group.aksrg.name
  }

  byte_length = 8
}

# Create storage account for boot diagnostics
resource "azurerm_storage_account" "storageaccount" {
  name                     = "diag${random_id.randomId.hex}"
  resource_group_name      = azurerm_resource_group.aksrg.name
  location                 = var.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags = var.tags
}

# Create AKS cluster
resource "azurerm_kubernetes_cluster" "akscluster" {
  count               = var.resource_count
  name                = "${var.cluster_name}-${count.index}"
  location            = var.location
  resource_group_name = azurerm_resource_group.aksrg.name
  dns_prefix          = var.aks_dns_prefix

  default_node_pool {
    name       = var.node_pool_name
    max_count = var.max_count
    min_count = var.min_count
    vm_size    = var.aks_agent_vm_size
    type       = "VirtualMachineScaleSets"
  }

  service_principal {
    client_id     = var.aks_client_id
    client_secret = var.aks_client_secret
  }
  depends_on = [azurerm_resource_group.aksrg]
  tags       = var.tags
}


# Create virtual machine
resource "azurerm_virtual_machine" "vmworkshop" {
  count                 = var.resource_count
  name                  = "${var.cluster_name}-${count.index}"
  location              = var.location
  resource_group_name   = azurerm_resource_group.aksrg.name
  network_interface_ids = [azurerm_network_interface.vmsnic[count.index].id]
  vm_size               = "Standard_DS1_v2"

  storage_os_disk {
    name              = "${var.cluster_name}-${count.index}-Disk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  os_profile {
    computer_name  = "${var.cluster_name}-${count.index}"
    admin_username = var.vm_username
    admin_password = var.vm_password
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  identity {
    type = "SystemAssigned"
  }

  boot_diagnostics {
    enabled     = "true"
    storage_uri = azurerm_storage_account.storageaccount.primary_blob_endpoint
  }
  depends_on = [azurerm_kubernetes_cluster.akscluster]
  tags = var.tags
}


# Copy kubeconfig into .kube folder instead of running az login
resource "null_resource" "aksvmkubeconfig" {
  count = var.resource_count
  depends_on = [azurerm_virtual_machine.vmworkshop]
  #https://github.com/hashicorp/terraform/issues/16330

  provisioner "remote-exec" {
    connection {
      type     = "ssh"
      host     = azurerm_public_ip.vmspublicip[count.index].ip_address
      user     = var.vm_username
      password = var.vm_password
    }
    inline = [
      "mkdir /home/${var.vm_username}/.kube/"
    ]
  }
  provisioner "file" {
    connection {
      host     = azurerm_public_ip.vmspublicip[count.index].ip_address
      type     = "ssh"
      user     = var.vm_username
      password = var.vm_password
    }
    content     = azurerm_kubernetes_cluster.akscluster[count.index].kube_config_raw
    destination = "/home/${var.vm_username}/.kube/config"
  }
}

output "vms-public-ip" {
  value = azurerm_public_ip.vmspublicip.*.ip_address
}