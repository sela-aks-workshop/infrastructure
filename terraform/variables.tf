variable "cluster_name" {
  type        = string
  description = "The name of the aks cluster"
}

variable "resource_count" {
  type        = number
  description = "Count of aks cluster and virtual machines you want to create"
}

variable "vm_name" {
  type        = string
  description = "The name of the virtual machine"
}

variable "vm_username" {
  type        = string
  description = "The username for the virtual machine"
}

variable "vm_password" {
  type        = string
  description = "The password of the vm_username"
}

variable "vm_size" {
  description = "The size of the Virtual Machine"
  #https://docs.microsoft.com/en-us/azure/virtual-machines/linux/sizes-general
  default = "Standard_D2s_v3"
}

variable "resource_group_name" {
  type        = string
  description = "The name of the resource group"
}

variable "aks_dns_prefix" {
  description = "Optional DNS prefix to use with hosted Kubernetes API server FQDN."
  default     = "aks"
}

variable "aks_client_id" {
  type        = string
  description = "The Client ID for the Service Principal to use for this Managed Kubernetes Cluster"
}

variable "aks_client_secret" {
  type        = string
  description = "The Client Secret for the Service Principal to use for this Managed Kubernetes Cluster"
}

variable "location" {
  type        = string
  description = "The Azure Region in which all resources should be provisioned"
}

variable "node_pool_name" {
  type        = string
  description = "The name which should be used for the default Kubernetes Node Pool. Changing this forces a new resource to be created"
}

variable "max_count" {
  type        = number
  description = "Maximum number of nodes you want to create inside the aks node group"
  default = 3
}

variable "min_count" {
  type        = number
  description = "Minimum number of nodes you want to create inside the aks node group"
  default = 1
}

variable "aks_agent_vm_size" {
  type        = string
  description = "The size of each node used by AKS cluster"
  #https://docs.microsoft.com/en-us/azure/virtual-machines/linux/sizes-general
}

variable "subscription_id" {
  type        = string
  description = "The Azure Subscription ID"
  # az account list --o table
}

variable "tenant_id" {
  type        = string
  description = "The Azure Tenant ID"
  # az account get-access-token --query tenant --output tsv
}

variable "tags" {
  type = object({ Environment=string})
  description = "Tags for identifier the resources created using terraform"
  default = {
    Environment = "sela-aks-workshop"
    }
}
