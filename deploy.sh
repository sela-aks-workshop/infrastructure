#!/bin/bash

# exit when any command fails
set -e

# Login with Azure CLI
echo "-------------------------------"
echo "Step 1: Login to Azure"
Login=$(az login -u ${AZURE_USERNAME} -p ${AZURE_PASSWORD})
az account set --subscription ${AZURE_SUBSCRIPTION_ID}

# Create Service Principal
echo "-------------------------------"
echo "Step 2: Set Service Principal"
ServicePrincipal=$(az ad sp create-for-rbac -n ${AZURE_SERVICE_PRINCIPAL_NAME} --skip-assignment)
sleep 5
az role assignment create --assignee $(echo $ServicePrincipal | jq -r '.appId')  --role Contributor --scope /subscriptions/${AZURE_SUBSCRIPTION_ID}

# Delete Terraform State File (If Exists)
echo "-------------------------------"
echo "Step 3: Delete Terraform State File (If Exists)"
rm -f /terraform/terraform.tfstate || true
rm -f /terraform/aks-workshop.tfvars || true
rm -f vms-public-ips.txt 2> /dev/null || true

# Create Terraform Configuration
echo "-------------------------------"
echo "Step 4: Create Terraform Configuration"
echo -e "cluster_name = \"aksworkshop\"" > /terraform/aks-workshop.tfvars
echo -e "node_pool_name = \"aksworkshop\"" >> /terraform/aks-workshop.tfvars
echo -e "max_count = 3" >> /terraform/aks-workshop.tfvars
echo -e "min_count = 1" >> /terraform/aks-workshop.tfvars
echo -e "aks_agent_vm_size = \"Standard_D1_v2\"" >> /terraform/aks-workshop.tfvars
echo -e "vm_name = \"aks-workstation\"" >> /terraform/aks-workshop.tfvars
echo -e "vm_username = \"sela\"" >> /terraform/aks-workshop.tfvars
echo -e "vm_password = \"Sela1234!\"" >> /terraform/aks-workshop.tfvars
echo -e "vm_size = \"Standard_D1_v2\"" >> /terraform/aks-workshop.tfvars
echo -e "resource_group_name = \"${AZURE_RESOURCE_GROUP_NAME}\"" >> /terraform/aks-workshop.tfvars
echo -e "location = \"${AZURE_LOCATION}\"" >> /terraform/aks-workshop.tfvars
echo -e "aks_client_id = \"$(echo $ServicePrincipal | jq -r '.appId')\"" >> /terraform/aks-workshop.tfvars
echo -e "aks_client_secret = \"$(echo $ServicePrincipal | jq -r '.password')\"" >> /terraform/aks-workshop.tfvars
echo -e "tenant_id = \"$(echo $ServicePrincipal | jq -r '.tenant')\"" >> /terraform/aks-workshop.tfvars
echo -e "subscription_id = \"${AZURE_SUBSCRIPTION_ID}\"" >> /terraform/aks-workshop.tfvars
echo -e "resource_count = \"${TOTAL_STUDENTS}\"" >> /terraform/aks-workshop.tfvars

echo "-------------------------------"
echo "Configuration:"
echo " "
cat /terraform/aks-workshop.tfvars
echo "-------------------------------"

# Move to Terraform Directory
echo "-------------------------------"
echo "Step 5: Move to Terraform Directory"
cd /terraform

# Init Terraform
echo "-------------------------------"
echo "Step 6: Init Terraform"
terraform init

# Test Terraform Template
echo "-------------------------------"
echo "Step 7: Test Terraform Template"
terraform plan -var-file="aks-workshop.tfvars"

# Apply Terraform Template
echo "-------------------------------"
echo "Step 8: Apply Terraform Template (Deploy Resources)"
terraform apply -var-file="aks-workshop.tfvars" -auto-approve

# Write VM`s Public Ips to file
echo "-------------------------------"
echo "Step 9:Write VMs Public Ips to file"
terraform output "vms-public-ip" >> vms-public-ips.txt


# Confirmation Message
echo "-------------------------------"
echo "AKS Workshop Environment Successfully Created (${TOTAL_STUDENTS} Clusters)"
echo "-------------------------------"